package aps.sorter;

import java.util.concurrent.TimeUnit;

import aps.scenario.Scenario;
import aps.util.Specifier;

@SuppressWarnings({"rawtypes", "unchecked"})
public abstract class Sorter<T> {
	private long executionNanoseconds;
	private String methodName;
	protected Scenario scenario;
	protected Specifier<T> specifier;
	protected T[] scenarioArray;
	
	public Sorter(String methodName, Scenario<T> scenario) {
		this.methodName = methodName;
		this.scenario = scenario;
		this.specifier = scenario.getSpecifier();
		this.scenarioArray = scenario.getArray();
	}
	
	public T[] apply() {
		long start = System.nanoTime();
		T[] result = execute();
		long end = System.nanoTime();
		executionNanoseconds = end - start;
		return result;
	}
	
	public void applyAndSave() {
		scenario.writeArray(apply(), methodName);
	}
	
	public String getExecutionDetail() {
		return methodName + ", scenario " + scenario.getScenarioName() + ": \r\n" + getExecutionTime();
	}
	
	private String getExecutionTime() {
		if(executionNanoseconds == 0)
			throw new UnsupportedOperationException("Executor methos not runned to define execution time");
		return TimeUnit.NANOSECONDS.toMillis(executionNanoseconds) + " milliseconds";
	}
	
	protected abstract T[] execute();

}