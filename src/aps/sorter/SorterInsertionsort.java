package aps.sorter;

import aps.scenario.Scenario;

public class SorterInsertionsort<T> extends Sorter<T> {

	public SorterInsertionsort(Scenario<T> scenario) {
		super("insertionsort", scenario);
	}

	@Override
	protected T[] execute() {
		return insertionSort(scenarioArray);
	}

	private T[] insertionSort(T[] arr)
	{
		T x;
		int j;
        for(int i = 1; i < arr.length; i++){
           x = arr[i];
           j = i - 1;
           while(j >= 0 && specifier.biggerThan(arr[j], x)){
              arr[j + 1] = arr[j];
              j -= 1;
           }
           arr[j + 1] = x; 
        }
        
        return arr;
	}
}
