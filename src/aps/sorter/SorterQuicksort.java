package aps.sorter;

import aps.scenario.Scenario;

public class SorterQuicksort<T> extends Sorter<T> {

	public SorterQuicksort(Scenario<T> scenario) {
		super("quicksort", scenario);
	}

	@Override
	protected T[] execute() {
		return quickSort(scenarioArray, 0, scenarioArray.length - 1);
	}

	private T[] quickSort(T[] arr, int init, int end) {
		int i = init, e = end;
		int iPivo = (init + end) / 2;
		T pivo = arr[iPivo];
		while(i < e) {
        	while(specifier.lessThan(arr[i], pivo)) i++;
        	while(specifier.biggerThan(arr[e], pivo)) e--;
        	if(i > e) continue;
        	T aux = arr[i];
        	arr[i] = arr[e];
        	arr[e] = aux;
        	i++; e--;
        }

        if(e > init) arr = quickSort(arr, init, e);
        if(i < end) arr = quickSort(arr, i, end);
        
        return arr;
  }
	
}