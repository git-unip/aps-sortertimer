package aps.sorter;

import aps.scenario.Scenario;

public class SorterBubblesort<T> extends Sorter<T> {

	public SorterBubblesort(Scenario<T> scenario) {
		super("bubblesort", scenario);
	}

	@Override
	protected T[] execute() {
		return bubbleSort(scenarioArray);
	}
	
	private T[] bubbleSort(T[] arr)
	{
		for(int ultimo = arr.length - 1; ultimo > 0; ultimo--)
            for(int i=0;i<ultimo;i++)
                if(specifier.biggerThan(arr[i], arr[i + 1])){
                	T aux = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = aux;
                }
		
		return arr;
	}

}