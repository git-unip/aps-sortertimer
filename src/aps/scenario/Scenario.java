package aps.scenario;

import aps.util.Filer;
import aps.util.Specifier;

@SuppressWarnings({ "rawtypes", "unchecked" })
public abstract class Scenario<T> extends Specifier<T> {
	
	protected String file = null;
	public Scenario() { }
	public Scenario(String scenarioFile) { this.file = scenarioFile; }
	
	public void setFile(String file) {
		this.file = file;
	}
	
	public T[] getArray() {
		return file == null ? generateRandoms() : (T[])toArray(new Filer(this, file).read());
	}
	
	public void writeArray(T[] orderedArray, String sorter) {
		new Filer(this, file).write(orderedArray, sorter);
	}
	
	protected int randomInt(int min, int max) {
		return min + (int)(Math.random() * ((max - min) + 1));
	}
	
	public abstract String getScenarioName();
}