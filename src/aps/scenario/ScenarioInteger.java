package aps.scenario;

import java.util.LinkedList;

public class ScenarioInteger extends Scenario<Integer> {

	public ScenarioInteger() { super(); }
	public ScenarioInteger(String file) { super(file); }
	
	@Override
	public String getScenarioName() { return "integers"; }
	@Override
	public Integer convert(String item) { return Integer.parseInt(item); }
	
	@Override
	public boolean biggerThan(Integer left, Integer right) {
		return left > right;
	}
	
	@Override
	public boolean biggerOrEqualThan(Integer left, Integer right) {
		return left >= right;
	}

	@Override
	public boolean lessThan(Integer left, Integer right) {
		return left < right;
	}
	
	@Override
	public boolean lessOrEqualThan(Integer left, Integer right) {
		return left <= right;
	}

	@Override
	public Integer[] toArray(LinkedList<Integer> linkedList) {
		Integer[] arr = new Integer[linkedList.size()];
		return linkedList.toArray(arr);
	}

	@Override
	public Integer[] generateRandoms() {
		Integer[] randoms = new Integer[randomRepeats];		
		for(int i = 0; i < randomRepeats; i++)
			randoms[i] = -randomInt(0, Integer.MAX_VALUE);

		return randoms;
	}

}