package aps.scenario;

import java.util.LinkedList;

public class ScenarioCharacter extends Scenario<Character> {

	public ScenarioCharacter() { super(); }
	public ScenarioCharacter(String file) {	super(file); }
	
	@Override
	public String getScenarioName() { return "characters"; }
	@Override
	public Character convert(String item) {
		if(item.length() != 1) throw new IllegalArgumentException("A string deve conter apenas um caracter");
		return item.charAt(0);
	}
	
	@Override
	public boolean biggerThan(Character left, Character right) {
		return left.compareTo(right) > 0;
	}
	
	@Override
	public boolean biggerOrEqualThan(Character left, Character right) {
		return left.compareTo(right) >= 0;
	}

	@Override
	public boolean lessThan(Character left, Character right) {
		return left.compareTo(right) < 0;
	}
	
	@Override
	public boolean lessOrEqualThan(Character left, Character right) {
		return left.compareTo(right) <= 0;
	}

	@Override
	public Character[] toArray(LinkedList<Character> linkedList) {
		Character[] arr = new Character[linkedList.size()];
		return linkedList.toArray(arr);
	}

	@Override
	public Character[] generateRandoms() {
		Character[] randoms = new Character[randomRepeats];		
		for(int i = 0; i < randomRepeats; i++)
			randoms[i] = (char)(randomInt(97, 122));

		return randoms;
	}

}