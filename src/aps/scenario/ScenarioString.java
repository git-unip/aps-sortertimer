package aps.scenario;

import java.util.LinkedList;

import aps.main.MainConfig;

public class ScenarioString extends Scenario<String> {

	public ScenarioString() { super(); }
	public ScenarioString(String file) { super(file); }
	
	@Override
	public String getScenarioName() { return "strings"; }
	@Override
	public String convert(String item) { return item; }
	
	@Override
	public boolean biggerThan(String left, String right) {
		return left.compareToIgnoreCase(right) > 0;
	}
	
	@Override
	public boolean biggerOrEqualThan(String left, String right) {
		return left.compareToIgnoreCase(right) >= 0;
	}

	@Override
	public boolean lessThan(String left, String right) {
		return left.compareToIgnoreCase(right) < 0;
	}
	
	@Override
	public boolean lessOrEqualThan(String left, String right) {
		return left.compareToIgnoreCase(right) <= 0;
	}	

	@Override
	public String[] toArray(LinkedList<String> linkedList) {
		String[] arr = new String[linkedList.size()];
		return linkedList.toArray(arr);
	}

	@Override
	public String[] generateRandoms() {
		String[] randoms = new String[randomRepeats];
		int stringLength = MainConfig.getInteger("randomstringlength");
		for(int i = 0; i < randomRepeats; i++) {
			char[] current = new  char[stringLength];
			for(int s = 0; s < stringLength; s++)
				current[s] = (char)(randomInt(97, 122));
			randoms[i] = new String(current);
		}

		return randoms;
	}

}