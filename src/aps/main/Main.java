package aps.main;

import java.util.Arrays;
import java.util.LinkedList;

import aps.scenario.Scenario;
import aps.scenario.ScenarioCharacter;
import aps.scenario.ScenarioInteger;
import aps.scenario.ScenarioString;
import aps.sorter.Sorter;
import aps.sorter.SorterBubblesort;
import aps.sorter.SorterInsertionsort;
import aps.sorter.SorterQuicksort;
import aps.util.Printer;

@SuppressWarnings({"rawtypes", "unchecked"})
public class Main {

	public static void main(String[] args) {
		new MainScreen();
		//startByMock();
	}
	
	private static void startByMock() {
		LinkedList<Scenario> scenarios = new LinkedList<Scenario>(Arrays.asList(
			//Exemplo para ler a cole��o de um arquivo(ououtFile � gerado na mesma pasta)
			//new ScenarioCharacter(Paths.get("src", "resources", "characters.txt").toString()),
			new ScenarioCharacter(),
			new ScenarioInteger(),
			new ScenarioString()
		));
		LinkedList<Sorter> sorters = makeSorters(scenarios);		
		executeSorters(sorters);
	}
	
	private static LinkedList<Sorter> makeSorters(LinkedList<Scenario>scenarios){
		LinkedList<Sorter> sorters = new LinkedList<Sorter>();
		for(Scenario scenario:scenarios) {
			sorters.add(new SorterQuicksort(scenario));
			sorters.add(new SorterInsertionsort(scenario));
			sorters.add(new SorterBubblesort(scenario));
		}
		
		return sorters;
	}

	private static void executeSorters(LinkedList<Sorter>sorters) {
		for(Sorter sorter:sorters) {
			sorter.applyAndSave();
			Printer.print(sorter.getExecutionDetail());
		}
	}
	
}