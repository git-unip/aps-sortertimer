package aps.main;
import javax.swing.*;
import aps.scenario.Scenario;
import aps.scenario.ScenarioCharacter;
import aps.scenario.ScenarioInteger;
import aps.scenario.ScenarioString;
import aps.sorter.Sorter;
import aps.sorter.SorterBubblesort;
import aps.sorter.SorterInsertionsort;
import aps.sorter.SorterQuicksort;

import java.awt.*;
import java.awt.event.*;
import java.io.File;

@SuppressWarnings({ "rawtypes", "unchecked", "serial", "deprecation" })
public class MainScreen extends JFrame implements ActionListener {
	private JPanel p1, p2, p3;
	private JLabel jlMetodo, jlTipo, jlArquivo, jlSaida;
	private JComboBox jcbMetodos, jcbTipos;
	private JTextField jtfArquivo;
	private JTextArea jtaSaida;
	private JButton jbOrdenar, jbSair, jbArquivo;
	private String[] metodos = {"Bubble", "Insertion", "QuickSort" };
	private String[] tipos = {"Integer", "String", "Character" };
	private JFileChooser arquivo;
	private String caminhoArquivo;

	public MainScreen() {
		caminhoArquivo = "";
		p1 = new JPanel(new GridLayout(4, 2));
		p2 = new JPanel();
		p3 = new JPanel(new BorderLayout());
		jlMetodo = new JLabel("M�todo");
		jlTipo = new JLabel("Tipo");
		jlArquivo = new JLabel("Arquivo");
		jlSaida = new JLabel("Saidas");
		jcbMetodos = new JComboBox(metodos);
		jcbTipos = new JComboBox(tipos);
		jtfArquivo = new JTextField(25);
		jtfArquivo.disable();
		arquivo = new JFileChooser();
		jtaSaida = new JTextArea(2,2);
		jbOrdenar = new JButton("Ordenar");
		jbSair = new JButton("Sair");
		jbArquivo = new JButton("...");
		p1.add(jlMetodo);
		p1.add(jcbMetodos);
		p1.add(jlTipo);
		p1.add(jcbTipos);
		p1.add(jlSaida);
		p1.add(jtaSaida);
		p1.add(jbOrdenar);
		p1.add(jbSair);
		p2.add(jlArquivo);
		p2.add(jtfArquivo);
		p2.add(jbArquivo);
		p3.add("North", p2);
		p3.add("Center", p1);
		jbArquivo.addActionListener(this);
		jbOrdenar.addActionListener(this);
		jbSair.addActionListener(this);
		getContentPane().add(p3);
		setTitle("M�todos de Ordena��o");
		setSize(400, 200);
		setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbSair)
			exit();
		else if(e.getSource() == jbOrdenar)
			ordenar();
		else if (e.getSource() == jbArquivo) {
			arquivo.showOpenDialog(null);
			File arq = arquivo.getSelectedFile();
			if(arq != null)
				caminhoArquivo = arq.getAbsolutePath().replace("\\", "/");
			jtfArquivo.setText(caminhoArquivo);
		}
	}
	
	private void exit() {
		System.exit(0);
	}
	
	private void ordenar() {
		Scenario scenario = null;
		switch (jcbTipos.getSelectedIndex()) {
			case 0:
				scenario = new ScenarioInteger();
				break;
			case 1:
				scenario = new ScenarioString();
				break;
			case 2:
				scenario = new ScenarioCharacter();
				break;
			default: return;
		}
		
		if(caminhoArquivo != null && caminhoArquivo != "")
			scenario.setFile(caminhoArquivo);
		
		Sorter sorter = null;
		switch (jcbMetodos.getSelectedIndex()) {
			case 0:
				sorter = new SorterBubblesort(scenario);
				break;
			case 1:
				sorter = new SorterInsertionsort(scenario);
				break;	
			case 2:
				sorter = new SorterQuicksort(scenario);
				break;
			default: return;
		}
		
		sorter.applyAndSave();
		String details = sorter.getExecutionDetail();
		jtaSaida.setText(details);
	}
}