package aps.main;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Properties;

public class MainConfig {
		
	public static String getString(String key) {
		Properties prop = new Properties();
	    InputStream input = null;
	    try {
	        input = new FileInputStream(Paths.get("src", "aps", "main", "config.properties").toString());
	        prop.load(input);
	        return prop.getProperty(key);
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
	    
	    return null;
	}
	
	public static Integer getInteger(String key) {
		return Integer.parseInt(getString(key));
	}
}
