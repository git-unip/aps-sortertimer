package aps.util;

import java.util.LinkedList;
import aps.main.MainConfig;

public abstract class Specifier<T> {
	
	protected int randomRepeats = MainConfig.getInteger("randomrepeats");
	
	public abstract boolean biggerThan(T left, T right);
	public abstract boolean biggerOrEqualThan(T left, T right);
	public abstract boolean lessThan(T left, T right);
	public abstract boolean lessOrEqualThan(T left, T right);
	public abstract T convert(String item);
	public abstract T[] generateRandoms();
	public abstract T[] toArray(LinkedList<T> linkedList);
	
	public Specifier<T> getSpecifier() {
		return this;
	}
	
}