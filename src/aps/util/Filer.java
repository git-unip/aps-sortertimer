package aps.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.Scanner;

import aps.main.MainConfig;
import aps.scenario.Scenario;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class Filer<T> {
	
	private Scenario<T> scenario;
	private String originalFile = null;
	
	public Filer(Scenario scenario) {
		this.scenario = scenario;
	}
	
	public Filer(Scenario scenario, String file) {
		this.scenario = scenario;
		this.originalFile = file;
	}
	
	public LinkedList<T> read() {
		Scanner scan = null;
		try { scan = new Scanner(new FileReader(originalFile)); }
		catch (FileNotFoundException e) { e.printStackTrace(); System.exit(1); }
		LinkedList<T> arr = new LinkedList<T>();
		while(scan.hasNext()) {
			T newItem = scenario.convert(scan.next());
			arr.add(newItem);
		}

		return arr;
	}
	
	public void write(T[] orderedList, String sorter) {
		String fileExtension = MainConfig.getString("defaultfileextension");
		String outputFilePath = MainConfig.getString("defaultoutputpath");
		String outputFileName = scenario.getScenarioName();
		
		if(originalFile != null) {
			File fl = new File(originalFile);
			fileExtension = fl.getName().substring(fl.getName().lastIndexOf("."), fl.getName().length());
			outputFilePath = fl.getParent();
			outputFileName = fl.getName().substring(0, fl.getName().lastIndexOf("."));
		}
		
		String outputFile = outputFilePath + "\\" + outputFileName + "-" + sorter + "-ordered" + fileExtension;
		File file = new File(outputFile);
		if(file.exists())
			file.delete();
		
		try {
			PrintWriter printer = new PrintWriter(outputFile, "UTF-8");
			for(T item:orderedList)
				printer.println(item.toString());
			printer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
}